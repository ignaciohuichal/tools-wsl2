#!/bin/bash

# Actualizar la lista de paquetes e instalar dependencias
sudo apt update
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

# Descargar la clave GPG oficial de Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Agregar el repositorio de Docker a las fuentes de APT
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Actualizar la lista de paquetes con el nuevo repositorio
sudo apt update

# Instalar Docker Engine
sudo apt install -y docker-ce

# Agregar tu usuario al grupo "docker" para ejecutar comandos Docker sin sudo
sudo usermod -aG docker $USER

# Reiniciar el servicio de Docker para aplicar cambios
sudo service docker restart

# Verificar la instalación de Docker
docker --version

# Mensaje de confirmación
echo "Docker se ha instalado correctamente. Es posible que necesites cerrar sesión o reiniciar WSL para aplicar los cambios."
