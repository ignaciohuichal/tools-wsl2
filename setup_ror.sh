#!/bin/bash

# Actualizar los paquetes disponibles
sudo apt-get update

# Instalar Ruby usando asdf
sudo apt-get install -y curl gnupg build-essential # No C compiler found
sudo apt-get install build-essential zlib1g-dev libssl-dev libreadline-dev libffi-dev libxml2-dev libsqlite3-dev

git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.1
echo ". $HOME/.asdf/asdf.sh" >> ~/.zshrc
echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.zshrc
source ~/.zshrc

sudo apt-get update
sudo apt-get install -y curl gpg

asdf plugin add ruby https://github.com/asdf-vm/asdf-ruby.git
asdf install ruby 3.1.0

asdf global ruby 3.1.0

# An error occurred while installing pg (1.5.3)
sudo apt-get install libpq-dev

# Instalar Rails y Bundler
gem install rails bundler

source ~/.zshrc

sudo apt update
# sudo apt install nodejs npm

sudo npm install -g yarn

echo "Instalación completada"

# chmod +x install_tools.sh  Dar permisos de ejecución
# ./install_tools.sh Ejecutar el script