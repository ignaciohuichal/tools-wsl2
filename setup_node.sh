#!/bin/bash

sudo apt update


curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

source ~/.zshrc

# para que nvm se inicie automáticamente cada vez que abres una terminal Zsh:
# Agregarlo al archivo...
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # Esta línea carga nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # Esta línea carga la completación de comandos de nvm

source ~/.zshrc

# instalar la última versión de Node.js y npm usando nvm
nvm install node


# Install react native
npm install -g expo-cli
# expo init MyPostApp

# chmod +x file.sh
# ./file.sh Ejecutar el script