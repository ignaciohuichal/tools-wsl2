#!/bin/bash

# Actualizar paquetes
sudo apt update

# Instalar PostgreSQL
sudo apt install -y postgresql

# Iniciar el servicio de PostgreSQL
sudo service postgresql start

# Acceder a la consola de PostgreSQL y crear usuario y base de datos
sudo -u postgres psql -c "CREATE USER ignacio WITH PASSWORD 'huichal';"
sudo -u postgres psql -c "CREATE DATABASE unnamed_test OWNER ignacio;"

# Salir de la consola de PostgreSQL
sudo -u postgres psql -c "\q"

# chmod +x setup_postgres.sh
# sudo service postgresql start

# sudo -u postgres psql
# CREATE DATABASE unnamed_test OWNER ignacio;
# \l show database
# ALTER USER ignacio CREATEDB;