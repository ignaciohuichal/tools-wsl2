#!/bin/bash

# Actualizar la lista de paquetes e instalar dependencias
sudo apt update
sudo apt install -y zsh wget git

# Instalar oh-my-zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Cambiar el shell por defecto a zsh
chsh -s $(which zsh)

# Mensaje para reiniciar la terminal y aplicar los cambios
echo "La instalación ha finalizado. Por favor, cierra esta terminal o reinicia WSL2 para usar zsh como tu shell predeterminado."

# chmod +x zsh.sh
# ./zsh.sh